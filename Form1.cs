﻿using OfficeOpenXml;
using System;
using System.Data;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace listStudent
{
    public partial class Form1 : Form
    {
        private DataTable dataTable = new DataTable();
        private CustomerModel context = new CustomerModel(); // CustomerModel là DbContext của bạn
        private int currentTT = 1;

        public Form1()
        {
            InitializeComponent();
            InitializeDataTable();
        }

        private void InitializeDataTable()
        {
            dataTable.Columns.Add("TT", typeof(int));
            dataTable.Columns.Add("Mã KH");
            dataTable.Columns.Add("Tên KH");
            dataTable.Columns.Add("Ngày Sinh", typeof(DateTime));
            dataTable.Columns.Add("SDT");
            dataTable.Columns.Add("Email");
            dataTable.Columns.Add("Địa CHỈ");

            dataGridView1.DataSource = dataTable;
        }

        private void LoadExcelData(string filePath)
        {
            using (var package = new ExcelPackage(new FileInfo(filePath)))
            {
                ExcelWorksheet worksheet = package.Workbook.Worksheets[0]; // Lấy bảng tính đầu tiên

                for (int row = 2; row <= worksheet.Dimension.Rows; row++)
                {
                    string customerID = worksheet.Cells[row, 1].Text;
                    DataRow existingRow = dataTable.Rows.Cast<DataRow>().FirstOrDefault(r => r.Field<string>("Mã KH") == customerID);

                    if (existingRow != null)
                    {
                        // Nếu đã có mã khách hàng trong dataTable, cập nhật thông tin cho dòng đó thay vì thêm dòng mới
                        existingRow["Tên KH"] = worksheet.Cells[row, 2].Text;
                        UpdateDateOfBirth(existingRow, worksheet.Cells[row, 3].Text);
                        existingRow["SDT"] = worksheet.Cells[row, 4].Text;
                        existingRow["Email"] = worksheet.Cells[row, 5].Text;
                        existingRow["Địa CHỈ"] = worksheet.Cells[row, 6].Text;
                    }
                    else
                    {
                        // Nếu chưa có mã khách hàng trong dataTable, thêm dòng mới
                        DataRow dataRow = dataTable.NewRow();
                        dataRow["TT"] = currentTT++;
                        dataRow["Mã KH"] = customerID;
                        dataRow["Tên KH"] = worksheet.Cells[row, 2].Text;
                        UpdateDateOfBirth(dataRow, worksheet.Cells[row, 3].Text);
                        dataRow["SDT"] = worksheet.Cells[row, 4].Text;
                        dataRow["Email"] = worksheet.Cells[row, 5].Text;
                        dataRow["Địa CHỈ"] = worksheet.Cells[row, 6].Text;
                        dataTable.Rows.Add(dataRow);
                    }
                }
            }
        }

        private void UpdateDateOfBirth(DataRow row, string dateString)
        {
            DateTime ngaySinh;
            if (DateTime.TryParseExact(dateString, new[] { "dd/MM/yyyy", "d/M/yyyy", "dd/MM/yyyy HH:mm:ss", "d/M/yyyy HH:mm:ss" },
                System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out ngaySinh))
            {
                row["Ngày Sinh"] = ngaySinh;
            }
            else
            {
                row["Ngày Sinh"] = DBNull.Value;
            }
        }

        private void UpdateDatabaseFromDataTable()
        {
            try
            {
                var context = new CustomerModel();
                using (var consaction = context.Database.BeginTransaction())
                {
                    try
                    {

                        foreach (DataRow dataRow in dataTable.Rows)
                        {
                            string customerID = dataRow["Mã KH"].ToString();
                            Customer existingCustomer = context.Customers.FirstOrDefault(p => p.CustomerID == customerID);

                            if (existingCustomer != null)
                            {
                                // Cập nhật thông tin cho khách hàng đã tồn tại
                                existingCustomer.Name = dataRow["Tên KH"].ToString();
                                if (dataRow["Ngày Sinh"] != DBNull.Value)
                                {
                                    existingCustomer.BirthDay = (DateTime)dataRow["Ngày Sinh"];
                                }
                                existingCustomer.SDT = dataRow["SDT"].ToString();
                                existingCustomer.Email = dataRow["Email"].ToString();
                                existingCustomer.Address = dataRow["Địa CHỈ"].ToString();
                            }
                            else
                            {
                                // Thêm mới khách hàng nếu chưa tồn tại
                                Customer newCustomer = new Customer
                                {
                                    CustomerID = customerID,
                                    Name = dataRow["Tên KH"].ToString()
                                };

                                if (dataRow["Ngày Sinh"] != DBNull.Value)
                                {
                                    newCustomer.BirthDay = (DateTime)dataRow["Ngày Sinh"];
                                }

                                newCustomer.SDT = dataRow["SDT"].ToString();
                                newCustomer.Email = dataRow["Email"].ToString();
                                newCustomer.Address = dataRow["Địa CHỈ"].ToString();
                                context.Customers.Add(newCustomer);
                            }
                        }

                        // Lưu các thay đổi vào cơ sở dữ liệu
                        context.SaveChanges();
                        consaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                        consaction.Rollback();
                    }

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnImport_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Excel Files|*.xlsx";
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                string filePath = openFileDialog.FileName;
                LoadExcelData(filePath);
            }
        }

        private void btnImport2_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Excel Files|*.xlsx";
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                string filePath = openFileDialog.FileName;
                LoadExcelData(filePath);
            }
        }

    }
}
